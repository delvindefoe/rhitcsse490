var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), // mongo db connection
    bodyParser = require('body-parser'), // parses info from post
    methodOverride = require('method-override'); // used to manipulate post data

    router.use(bodyParser.urlencoded({extended: true}));
    router.use(methodOverride(function(req, res){
        if(req.body && typeof req.body == 'object' && '_method' in req.body){
            // look in urlencoded POST bodies and delete it
            var method = req.body._method;
            delete req.body._method;
            return method;
        }
    }));

    // Build the REST operatioins at the base of contacts
    // This will be accessible from localhost:9999/contacts 
    router.route('/')
        // GET all contacts
        .get(function(req, res, next){
            //  retrieve all the contacts from the db
            mongoose.model('Contact').find({}, function(err, contacts){
                if(err){
                    return  console.error(err);
                } else {
                    res.format({
                        json: function(){
                            res.json(contacts);
                        }
                    });
                }
            });
        })
        // Post a new contact
        .post(function(req, res){
            mongoose.model('Contact').create({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                homePhone: req.body.homePhone,
                cellPhone: req.body.cellPhone,
                birthDay: req.body.birthDay,
                website: req.body.website,
                address: req.body.address
            }, function(err, contact) {
                if (err){
                    res.send("There was a problem adding contact to the db");
                } else {
                    res.format({
                        json: function(){
                            res.json(contact);
                        }
                    });
                }
            });
        });

     // route middleware to validata :id
    router.param('id', function(req, res, next, id){
        mongoose.model('Contact').findById(id, function(err, contact){
            if(err || contact === null){
                res.status(404);
                err = new Error('Not Found');
                err.status = 404;
                res.format({
                    // html: function(){
                    //     next(err);
                    // },
                    json: function(){
                        res.json({message: err.status + ' ' + err});
                    }
                });
            } else{
                // once validation is done, save new id in the req
                req.id = id;
                next();
            }
        });
    });

    router.route('/:id')
        .get(function(req, res){
            mongoose.model('Contact').findById(req.id, function(err, contact){
                if(err){
                    res.status(404);
                    err = new Error('GET error, problem retrieving data');
                    err.status = 404;
                    res.format({
                        json: function() {
                            res.json({message: err.status + ' ' + err});
                        }
                    });
                } else {
                    res.format({
                        json: function(){
                            res.json(contact);
                        }
                    });
                }
            });
        })
        .put(function(req, res){
            mongoose.model('Contact').findById(req.id, function(err, contact){
                contact.firstName = req.body.firstName;
                contact.lastName = req.body.lastName;
                contact.email =  req.body.email;
                contact.homePhone = req.body.homePhone;
                contact.cellPhone = req.body.cellPhone;
                contact.birthDay = req.body.birthDay;
                contact.website = req.body.website;
                contact.address = req.body.address;
                contact.save(function(err, person){
                    if(err){
                        res.status(404);
                        err = new Error('Problem updating contact');
                        err.status = 404;
                        res.format({
                            json: function() {
                                res.json({message: err.status + ' ' + err});
                            }
                        });
                    } else {
                        res.format({
                            json: function(){
                                res.json(person);
                            }
                        });
                    }
                });
            });
        })
        .delete(function(req, res){
            mongoose.model('Contact').findByIdAndRemove(req.id)
            .exec(
                function(err, contact){
                    if(err){
                        res.status(404);
                        err = new Error('Problem deleting contact');
                        err.status = 404;
                        res.format({
                            json: function(){
                                res.json({message : err.status + " " + err});
                            }
                        });
                    } else {
                        res.status(204);
                        console.log(contact);
                        res.format({
                            json: function(){
                                res.json(null);
                            }
                        });
                    }

                }
            );
        });

module.exports = router;