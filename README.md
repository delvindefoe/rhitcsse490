# README #


git clone https://bitbucket.org/delvindefoe/rhitcsse490.git to clone this repo on your local machine.  

Most of the small projects will have a package.json manifest that will include all the dependencies required for building their apps.

Look for a scripts section in the package.json file.  If one exists, run the appropriate command for running or testing the app. 

### What is this repository for? ###

* This is a repo for sharing class materials with students in [CSSE490-01](http://www.rose-hulman.edu/class/csse/csse490WebServicesDev/201620/Schedule/Schedule.htm).
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### Who do I talk to? ###

* Delvin Defoe -- [email](defoe@rose-hulman.edu)